package com.koval.minesweeper;

import org.apache.logging.log4j.*;

import java.util.Arrays;
import java.util.Random;

@SuppressWarnings("Duplicates")
public class VoidMineSweeper {

    private Logger logger = LogManager.getLogger(MineSweeper.class);
    public int numberOfNeighbour = 0;

    private int countMines(int x, int y, int n, int m, boolean[][] arr, int[][] count, boolean[][] used){
        if(x < 0 || y < 0) return 0;
        if(x >= n || y >= m) return 0;
        if(arr[x][y])return 1;
        if(used[x][y])return 0;
        used[x][y] = true;
        count[x][y] += countMines(x - 1, y, n, m, arr, count,used);
        count[x][y] += countMines(x - 1, y - 1, n, m, arr, count,used);
        count[x][y] += countMines(x - 1, y + 1, n, m, arr, count,used);
        count[x][y] += countMines(x, y - 1, n, m, arr, count,used);
        count[x][y] += countMines(x, y + 1, n, m, arr, count,used);
        count[x][y] += countMines(x + 1, y, n, m, arr, count,used);
        count[x][y] += countMines(x + 1, y - 1, n, m, arr, count,used);
        count[x][y] += countMines(x + 1, y + 1, n, m, arr, count,used);
        return 0;
    }

    public void mineSweeper(int n, int m, int probability)throws Exception{

        if(n*m <= probability){
            throw new Exception("Impossible constraits");
        }

        Random rd = new Random();
        boolean[][]mines = new boolean[n][m];
        for(boolean[] row: mines)
            Arrays.fill(row,false);

        boolean[][]used = new boolean[n][m];
        for (boolean[] row: used)
            Arrays.fill(row,false);

        int x,y;
        for (int i = 0; i < probability; i++) {
            x = rd.nextInt(n);
            y = rd.nextInt(m);
            if(!used[x][y]){
                used[x][y] = true;
                mines[x][y] = true;
            }else i--;
        }

        logger.trace("The filed with mines :\n");
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if(mines[i][j])
                    logger.trace("†" + " ");
                else
                    logger.trace("•" + " ");
            }
            logger.trace("\n");
        }
        logger.trace("\n† - mine " + "\n" +
                "• - safe cell" + "\n");

        int[][]count = new int[n][m];
        for (int[]row:count)
            Arrays.fill(row,0);

        boolean[][]functional = new boolean[n][m];
        for (boolean[]row: functional)
            Arrays.fill(row,false);

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if(!functional[i][j]){
                    countMines(i,j,n,m,mines,count,functional);
                }
            }
        }

        logger.trace("\n");
        logger.trace("Field with calculated number of neighbour mines for each safe cell:\n");
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                numberOfNeighbour += count[i][j];
                if(count[i][j] == 0)
                    logger.trace(" " + "†" + " ");
                else
                    logger.trace(" " + count[i][j] + " ");
            }
            logger.trace("\n");
        }
    }
}
