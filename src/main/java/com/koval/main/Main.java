package com.koval;

import com.koval.minesweeper.VoidMineSweeper;
import com.koval.plateau.Plateau;
import com.sun.istack.internal.NotNull;
import org.apache.logging.log4j.*;

import java.util.Arrays;

public class Main {
    private static Logger logger = LogManager.getLogger(Main.class);

    public static void main(@NotNull String[] args)throws Exception {
        logger.trace("\n~~~PLATEAU TASK~~~\n");
        int[] arr = new int[]{12, 12, 13, 13, 13, 13, 13, 7, 7, 7, 7, 6};
        logger.trace("Sample array: " + Arrays.toString(arr) + "\n");
        new Plateau(arr.length,arr).plateau(arr.length,arr);

        logger.trace("\n~~~MINESWEEPER TASK~~~\n");
        int n = Integer.parseInt(args[0]);
        int m = Integer.parseInt(args[1]);
        int possibility = Integer.parseInt(args[2]);
        new VoidMineSweeper().mineSweeper(n,m,possibility);

    }
}
