package com.koval.plateau;

import java.util.Stack;
import org.apache.logging.log4j.*;
public class Plateau {

    private Logger logger = LogManager.getLogger(Plateau.class);
    public int size;
    public int[]sequence;

    public Plateau(int size, int[]sequence){
        this.size = size;
        this.sequence = sequence;
    }

    public int plateau(int size, int[]sequence) {
        Stack<Integer> integerStack = new Stack<>();
        int maxStartIndex = 0;
        int maxEndIndex = 0;
        int start;
        int end;
        int maxTemporary = -1;
        boolean check = true;

        for (int i = 0; i < sequence.length - 1; i++) {
            if (sequence[i] != sequence[i + 1]) {
                check = false;
                break;
            }
        }

        if (check){
            logger.trace("Whole array is a suitable sequence");
            return sequence.length;
        }
        else {
            for (int i = 1; i < size; i++) {

                if (sequence[i] == sequence[i - 1]) {
                    integerStack.push(i);
                } else {
                    if (!integerStack.empty()) {
                        start = integerStack.peek() - integerStack.size();
                        end = integerStack.peek();

                        if (integerStack.size() + 1 > maxTemporary && start - 1 >= 0 && end + 1 <= size - 1 &&
                                sequence[start - 1] < sequence[start] && sequence[end + 1] < sequence[end]) {
                            maxTemporary = integerStack.size() + 1;
                            maxStartIndex = start;
                            maxEndIndex = end;
                            integerStack.clear();
                        } else if (start - 1 >= 0 && end + 1 > size - 1 && sequence[start - 1] < sequence[start]
                                && integerStack.size() + 1 > maxTemporary) {
                            maxTemporary = integerStack.size() + 1;
                            maxStartIndex = start;
                            maxEndIndex = end;
                            integerStack.clear();
                        } else if (start - 1 < 0 && end + 1 <= size - 1 && sequence[end + 1] < sequence[end] &&
                                integerStack.size() + 1 > maxTemporary) {
                            maxTemporary = integerStack.size() + 1;
                            maxStartIndex = start;
                            maxEndIndex = end;
                            integerStack.clear();
                        }
                    }
                    integerStack.clear();
                }
            }
            logger.trace("The longest suitable sequence starts from " + maxStartIndex +
                    " index to " + maxEndIndex + " index" + "\n");

            return maxEndIndex - maxStartIndex + 1;
        }
    }
}
