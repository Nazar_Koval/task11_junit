package com.koval.tests;

import com.koval.minesweeper.MineSweeper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.jupiter.api.RepeatedTest;
import static org.junit.jupiter.api.Assertions.*;

public class MineSweeperTests {

    private MineSweeper mineSweeper;
    private Logger logger = LogManager.getLogger(MineSweeper.class);

    @RepeatedTest(value = 5, name = "{currentRepetition}/{totalRepetitions}")
    @Test
    public void mineSweeperTest1()throws Exception{
        logger.trace("mineSweeperTest1\n");
        mineSweeper = new MineSweeper();
        int n = 5;
        int m = 5;
        int p = 10;
        int res = mineSweeper.mineSweeper(n,m,p);
        assertTrue(res >= p/2, "Test #1 failed");
    }

    @Test
    public void mineSweeperTest2(){
        logger.trace("mineSweeperTest2\n");
        int n = 2;
        int m = 2;
        int p = 4;
        mineSweeper = new MineSweeper();
        assertThrows(Exception.class, ()-> mineSweeper.mineSweeper(n,m,p), "Test #2 failed");
    }

    @RepeatedTest(value = 3, name = "{currentRepetition}/{totalRepetitions}")
    @Test
    public void mineSweeperTest3() {
        logger.trace("mineSweeperTest3\n");
        mineSweeper = new MineSweeper();
        int n = 2;
        int m = 2;
        int p = 1;
        try {
            assertEquals(p * 3, mineSweeper.mineSweeper(n, m, p), "Test #3 failed");
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }
}
