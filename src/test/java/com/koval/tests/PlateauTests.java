package com.koval.tests;

import com.koval.plateau.Plateau;
import org.junit.Test;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import static org.junit.jupiter.api.Assertions.*;

@RunWith(JUnitPlatform.class)
public class PlateauTests {

    @Test
    public void plateauTest1(){
        Plateau plateau = new Plateau(7,new int[]{1,1,1,2,1,1,1});
        int res = plateau.plateau(plateau.size,plateau.sequence);
        assertEquals(1,res, "Test #1 failed");
    }

    @Test
    public void plateauTest2(){
        int size = 10;
        int[]arr = new int[]{1,2,3,4,5,6,7,8,9,10};
        int res = new Plateau(size,arr).plateau(size,arr);
        assertEquals(1,res, "Test #2 failed");
    }

    @Test
    public void plateauTest3(){
        int[] arr = new int[]{12, 12, 13, 13, 13, 13, 13, 7, 7, 7, 7, 6};
        Plateau plateau = new Plateau(arr.length, arr);
        int res = plateau.plateau(plateau.size,plateau.sequence);
        assertEquals(5,res, "Test #3 failed");
    }

    @Test
    public void plateauTest4(){
        int size = 77;
        int[]arr = new int[size];
        assertEquals(size, new Plateau(size,arr).plateau(size,arr), "Test #4 failed");
    }
}
