package com.koval.tests;

import com.koval.minesweeper.VoidMineSweeper;
import org.junit.Test;
import static org.junit.jupiter.api.Assertions.*;

public class VoidMineSweeperTest {

    @Test
    public void voidTest1()throws Exception{
        int n = 5;
        int m = 5;
        int p = 10;
        VoidMineSweeper voidMineSweeper = new VoidMineSweeper();
        voidMineSweeper.mineSweeper(n,m,p);
        assertTrue(voidMineSweeper.numberOfNeighbour > p/2);
    }

    @Test
    public void voidTest2(){
        VoidMineSweeper voidMineSweeper = new VoidMineSweeper();
        int n = 2;
        int m = 2;
        int p = 1;
        try {
            voidMineSweeper.mineSweeper(n,m,p);
        }catch (Exception exc){
            exc.printStackTrace();
        }
        assertEquals(p*3, voidMineSweeper.numberOfNeighbour);
    }
}
